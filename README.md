## 包结构

### common

`ResultResponse`:统一响应返回体

### rest

通过客户端请求获取客户端ip归属地的控制层

### service

实现层，具体业务代码

### util

包含主要工具类`IPUtils`，通过请求体获取IP归属地的核心实现。

## 项目使用

本项目基于Spring Boot通过开源项目`ip2region`提供的`API`，实现了获取请求归属地的功能。

> `ip2region`项目地址：
>
> [Ip2region]: https://gitee.com/lionsoul/ip2region.git
>
> ip2region - 是一个离线IP地址定位库和IP定位数据管理框架，10微秒级别的查询效率，提供了众多主流编程语言的 `xdb` 数据生成和查询客户端实现。

使用前需要将ip2region.xdb下载到本地，然后将该文件放置在您服务器/本地合适的位置，确保文件可以正确被程序加载到。

问中加载路径：

```java
private static final String DB_PATH = "/root/home_place/ip2region.xdb";
```

在此，我更推荐使用`yml`或其他配置文件的方式配置，以便修改。

### 请求参数

使用`javax.servlet.http.HttpServletRequest`作为请求参数即可，不需要任何处理，调用IPUtils的`getIPRegion`方法即可得到ip归属地信息。

```java
String ipRegion = IPUtils.getIPRegion(request);
```

### 返回值

#### 接口返回值

```json
{
	"success": true,
	"trace": "023c71f9-f483-466d-b650-a30fa097b64c",
	"code": "OK",
	"message": "获取成功",
	"data": "中国|0|山东省|青岛市|移动"
}
```

#### 方法返回值

> 中国|0|山东省|青岛市|移动

### 压测

压测环境：

- 2C 2T
- 2G内存
- 3M

压测工具：

ApiPost 7

并发数：100

时长：10S

![image-20231129110328475](C:\Users\Lt'x\AppData\Roaming\Typora\typora-user-images\image-20231129110328475.png)



## 致谢

感谢大佬 **狮子的魂** 无私的开源奉献精神，让我们能如此方便的实现功能。

我们每个人都是站在巨人的肩膀上。