package com.personal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author Liutx
 * @since 2023-11-28 10:54
 */
@EnableAsync
@EnableAspectJAutoProxy
@SpringBootApplication
public class HomePlaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomePlaceApplication.class, args);
	}
}