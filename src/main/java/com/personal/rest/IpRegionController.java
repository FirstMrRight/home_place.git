package com.personal.rest;

import com.personal.common.ResultResponse;
import com.personal.service.IpRegionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Liutx
 * @since 2023-11-28 10:41
 */
@RequestMapping("/api")
@RestController
public class IpRegionController {

	private final IpRegionService ipRegionService;

	public IpRegionController(IpRegionService ipRegionService) {
		this.ipRegionService = ipRegionService;
	}

	@GetMapping("/ip/region")
	public ResultResponse<String> region(HttpServletRequest request) {
		return ipRegionService.getRegionByRequest(request);
	}
}