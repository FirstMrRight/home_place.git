package com.personal.rest;

import com.personal.common.ResultResponse;
import com.personal.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Liutx
 * @since 2023-12-01 14:17
 */
@RestController
@RequestMapping("/order")
public class OrderController {

	private final OrderService orderService;

	public OrderController(OrderService orderService) {
		this.orderService = orderService;
	}

	@GetMapping("order_no")
	public ResultResponse<String> orderNo() {
		return orderService.orderNo();
	}
}