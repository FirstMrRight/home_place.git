package com.personal.service;

import com.personal.common.ResultResponse;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Liutx
 * @since 2023-11-28 10:38
 */
public interface IpRegionService {

	/**
	 * 通过客户端请求获取请求归属地
	 *
	 * @param request 客户端请求对象
	 * @return 客户端归属地
	 */
	ResultResponse<String> getRegionByRequest(HttpServletRequest request);
}