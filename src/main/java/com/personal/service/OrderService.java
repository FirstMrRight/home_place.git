package com.personal.service;

import com.personal.common.ResultResponse;

/**
 * @author Liutx
 * @since 2023-12-01 14:17
 */
public interface OrderService {

	/**
	 * 生成一个订单号
	 *
	 * @return 订单号
	 */
	ResultResponse<String> orderNo();

}
