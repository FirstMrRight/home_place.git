package com.personal.service.impl;

import com.personal.common.ResultResponse;
import com.personal.service.IpRegionService;
import com.personal.utils.IPUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * 通过请求获取用户IP归属地信息
 *
 * @author Liutx
 * @since 2023-11-28 10:38
 */
@Slf4j
@Service
public class IpRegionServiceImpl implements IpRegionService {

	/**
	 * 通过客户端请求获取请求归属地
	 *
	 * @param request 客户端请求对象
	 * @return 客户端归属地
	 */
	@Override
	public ResultResponse<String> getRegionByRequest(HttpServletRequest request) {
		ResultResponse<String> resultResponse = ResultResponse.newSuccessInstance();
		String ipRegion = IPUtils.getIPRegion(request);
		return resultResponse.code(HttpStatus.OK).data(ipRegion).message("获取成功");
	}
}