package com.personal.service.impl;

import com.personal.common.ResultResponse;
import com.personal.common.gen.GenOrderCode;
import com.personal.service.OrderService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author Liutx
 * @since 2023-12-01 14:17
 */
@Service
public class OrderServiceImpl implements OrderService {

	private final GenOrderCode genOrderCode;

	public OrderServiceImpl(GenOrderCode genOrderCode) {
		this.genOrderCode = genOrderCode;
	}

	@Override
	public ResultResponse<String> orderNo() {
		ResultResponse<String> resultResponse = ResultResponse.newSuccessInstance();
		LocalDate currentDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMdd");
		String orderPrefix = currentDate.format(formatter);
		//redisKey = Enum + ":" + orderPrefix;=> order:231201，每天的key都不一样，生成的订单号以天区分
		String key = "com" + orderPrefix;
		String orderCode = genOrderCode.genOrderCode(25, orderPrefix, key);
		return resultResponse.succeed().data(orderCode);
	}
}