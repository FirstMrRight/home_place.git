import lombok.extern.slf4j.Slf4j;
import org.lionsoul.ip2region.xdb.Searcher;

import java.util.concurrent.TimeUnit;

/**
 * @author Liutx
 * @since 2023-11-28 10:12
 */
@Slf4j
public class SearcherTest {
	public static void main(String[] args) {

		String ip = "192.168.31.1";
		try {
			// 1、创建 searcher 对象
			String dbPath = "src/main/resources/ipdata/ip2region.xdb";
			Searcher searcher = null;
			searcher = Searcher.newWithFileOnly(dbPath);
			// 2、查询
			long sTime = System.nanoTime();
			String region = searcher.search(ip);
			long cost = TimeUnit.NANOSECONDS.toMicros((long) (System.nanoTime() - sTime));
			log.info("{region: {}, ioCount: {}, took: {} μs}", region, searcher.getIOCount(), cost);
			// 3、关闭资源
			searcher.close();
			// 备注：并发使用，每个线程需要创建一个独立的 searcher 对象单独使用。
		} catch (Exception e) {
			log.error("IP:{}获取IP归属地错误，错误原因:", ip, e);
		}
	}
}